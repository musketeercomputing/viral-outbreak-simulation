## Modifying Settings

In the **settings.py** file, you can toggle the settings of the simulation. Below are the definitions of each of the parameters.

**POPULATION_X**: Width of population (in context of a social network); total population will be the area of the social network (X*Y)

**POPULATION_Y**: Height of population (in context of a social network); total population will be the area of the social network (X*Y)

**TRANSMISSION_RATE**: Probability that person A (healthy) gets infected upon coming into contact with person B (infected)

**SHOW_SYMPTOMS_AFTER**: Numbers of days after which symptoms begin to show for an infected person

**SOCIAL_BOUNDARY**: The social 'reach' of a person; people in the simulation will NEVER come into contact with anyone outside of their social boundary (i.e. if social boundary is 5, then people in the simulation will not come into contact with anyone who has a social distance of more than 5 from them)

**SOCIAL_DISTANCE**: Social distance between any 2 persons in the simulation

**INITIAL_INFECTED_PERSONS**: Number of persons infected at beginning (Day 0) of simulation

**PERCENT_TESTED_AFTER_SYMPTOMS**: Percent of people who get tested after symptoms show

**PERCENT_SELF_QUARANTINE_AFTER_SYMPTOM**: Percent of people who self-quarantine after experiencing symptoms

**SIMULATION_DAYS**: Number of days to simulate

**OVERWRITE_RESULTS**: Will always overwrite the results file if set to True

---

## Running the simulation

Run **main.py** to begin the simulation. By default, simulation results will be stored in a csv file (name will be based on current date).