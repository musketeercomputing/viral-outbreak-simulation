from random import*
from settings import*
from datetime import datetime
import csv
import os

#Create a class so we can generate instances of "Person"
class Person:
    #define initial parameters for each person
    def __init__(self, x, y, social_distance, group='Main'):
        self.x = x
        self.y = y
        self.group = group
        self.social_distance = social_distance
        self.infected = 0
        self.showing_symptoms = 0
        self.tested = 0
        self.tested_positive = 0
        self.days_after_infected = 0
        self.neighbors = []

    #simulate a person coming in contact w/ an infected person; whether person becomes infected upon this contact is based on the transmission rate defined in SETTINGS
    def come_in_contact(self, rate):
        if random() <= rate:
            self.infected=1
            return True

    #Define a person's neighbors based on their social boundary; a person will NEVER come into contact with anyone outside of their 'social neighborhood'
    def add_neighbors(self, all, social_boundary=SOCIAL_BOUNDARY):
        for i in range(max([0,self.x-social_boundary]), min([POPULATION_X, self.x+social_boundary+1])):
            for j in range(max([0,self.y-social_boundary]), min([POPULATION_Y, self.y+social_boundary+1])):
                self.neighbors.append(all[i][j])

    #Update the person's status: if they are infected and not showing symptoms, they will begin showing symptoms after D days (defined in SETTINGS)
    def update(self):
        if self.infected and not self.showing_symptoms:
            if self.days_after_infected >= SHOW_SYMPTOMS_AFTER:
                self.showing_symptoms = 1
            self.days_after_infected += 1

        #if person has not decided whether to get tested yet and are showing symptoms, there's a chance they will get tested, in which case results will come back positive and they are labeled as 'Tested Positive'
        #if they decide not to get tested, we treat them as having already tested (negative), meaning they will not try to get tested again in the future
        if not self.tested and self.showing_symptoms:
            if random() <= PERCENT_TESTED_AFTER_SYMPTOMS:
                self.tested_positive = 1
            self.tested = 1


def simulate(number_infected, social_distance=1.0, group_name="", group_size=0, simulation_days=10):

    #Open csv file to store results
    filename = "simulation_results_{}.csv".format(str(datetime.now()).split(" ")[0])
    #Check if file exists; if yes, open in append mode; otherwise create new file
    if filename in os.listdir() and not OVERWRITE_RESULTS:
        csvfile = open(filename, "a", newline="\n")
        writer = csv.writer(csvfile)
        print("File already exists. Opening in append mode.")
    else:
        csvfile = open(filename, "w", newline="\n")
        writer = csv.writer(csvfile)
        #Write header row
        writer.writerow(["POPULATION SIZE", "TRANSMISSION RATE", "SHOW SYMPTOMS AFTER", "SOCIAL BOUNDARY", "SOCIAL DISTANCE",
                         "INITIAL INFECTED PERSONS", "PERCENT TESTED AFTER SYMPTOMS", "PERCENT SELF QUARANTINE AFTER SYMPTOM",
                         "SIMULATION DAYS", "DAY", "INFECTED POPULATION", "SYMPTOM POPULATION", "TESTED POSITIVE POPULATION"])

    print("Initializing simulation parameters...")
    #Define population, starting w/ empty population; later we will add instances of Person based on population size
    population = []
    #Define the social network for the population; this keeps track of social coordinates of each Person
    #This is helpful for defining the social neighborhood for each Person object, which will greatly reduce run time later
    population_social_neighborhood = [[[] for i in range(POPULATION_X)] for j in range(POPULATION_Y)]

    #Start with empty results dictionary; this is not actually needed if we are storing results into csv
    #but it helps if you want to print out results while the simulation is running
    results = {
        "group_infected_on_day": 0,
        "infected_population": 0,
        "symptom_population": 0,
        "tested_positive": 0
    }

    #Starting from day 1
    day = 1

    print("Creating population...")
    #Begin adding instances of Person into the population
    for x in range(POPULATION_X):
        for y in range(POPULATION_Y):
            person = Person(x, y, social_distance=social_distance)
            population.append(person)
            population_social_neighborhood[x][y] = person

    print("Defining social neighborhoods...")
    #Define each Person's social neighborhood
    for person in population:
        person.add_neighbors(population_social_neighborhood)

    print("Randomly selecting infected population...")
    #Randomly infect X people, based on the INITIAL_INFECTED_PERSONS parameter
    for i in range(number_infected):
        targ = choice(population)
        targ.infected=1

    #This is used if you want to track the likelihood of a person within a certain group getting infected; Ignore for now
    if group_name:
        for i in range(group_size):
            targ = choice(population)
            targ.group = group_name

    print("Running simulation...")
    #Begin simulation
    for i in range(simulation_days):
        contacted_population = []
        #Loop through each person in the population
        for person in population:
            #If person is not showing symptoms OR if they aren't in self-quarantine (due to mild symptoms, etc...)
            #And also if the person has not tested positive (we assume if they have been tested positive, they are in self-quarantine)
            if (not person.showing_symptoms or random() <= PERCENT_SELF_QUARANTINE_AFTER_SYMPTOM) and not person.tested_positive:
                #Check if person is infected
                if person.infected:
                    #If person is infected, then loop through person's social neighborhood and simulate contact with each person in social neighborhood
                    for pc in person.neighbors:
                        r = random()
                        d = distance(pc, person)
                        #If potential contact is within neighborhood and has not been infected yet (we ignore contact between 2 already infected people since it's pointless)
                        if d > 0 and d <= 5 and not pc.infected:
                            #The probability of them coming into contact is based on their social distance from each other; higher distance = lower chance of contact
                            if r <= 1/d**.8:
                                #Append contact to a list if the probability is met; this list keeps track of everyone who came into contact w/ an infected person in the simulated day
                                contacted_population.append(pc)
            #update the person: see 'update' method defined in Person class
            person.update()

        #loop through the people who came into contact w/ an infected person and determine if each one became infected
        for contact in contacted_population:
            infected = contact.come_in_contact(TRANSMISSION_RATE)
            #This is for group tracking; ignore for now
            if results["group_infected_on_day"] == 0 and infected and contact.group == group_name:
                results["group_infected_on_day"] = day

        #Calculate total number of infected people/symptomatic people/tested positive people at end of day
        infected_population = sum([person.infected for person in population])
        symptom_population = sum([person.showing_symptoms for person in population])
        tested_positive_population = sum([person.tested_positive for person in population])
        print("\n\n\n-------------------------------------\nDay ", day)
        print("{} out of {} infected.".format(infected_population, POPULATION_SIZE))
        print("{} out of {} showing symptoms, with ~{}% self quarantined.".format(symptom_population, POPULATION_SIZE, PERCENT_SELF_QUARANTINE_AFTER_SYMPTOM*100))
        print("{} out of {} tested positive and quarantined.".format(tested_positive_population, POPULATION_SIZE))
        print("Percent of population infected:", round(infected_population/POPULATION_SIZE*100,3), "%")
        print("Percent of population showing symptoms:", round(symptom_population /POPULATION_SIZE*100,3), "%")
        print("Percent of population tested positive:", round(tested_positive_population /POPULATION_SIZE*100,3), "%")

        #Write results to file
        writer.writerow([POPULATION_SIZE, TRANSMISSION_RATE, SHOW_SYMPTOMS_AFTER, SOCIAL_BOUNDARY, SOCIAL_DISTANCE,
                           INITIAL_INFECTED_PERSONS, PERCENT_TESTED_AFTER_SYMPTOMS, PERCENT_SELF_QUARANTINE_AFTER_SYMPTOM,
                           SIMULATION_DAYS, day, infected_population, symptom_population, tested_positive_population,])
        #Increment the day counter before simulating the next day
        day += 1


    results["infected_population"] = infected_population
    results["symptom_population"] =  symptom_population
    results["tested_positive"] =  tested_positive_population

    #Close file; print summary
    csvfile.close()
    print(results)
    return results


#Calculating distance between 2 Person objects based on their social coordinates
def distance(a, b):
    return (abs(a.x*a.social_distance - b.x*b.social_distance)**2 + abs(a.y*a.social_distance - b.y*b.social_distance)**2)**.5


#Ignore; helper function that's not used in actual simulation
def calculate_avg_contacts(radius):
    x = [Person(radius*i, radius*j) for i in range(100) for j in range(100)]
    targ = Person(50,50)
    total = 0
    for p in x:
        d = distance(p, targ)
        if d != 0:
            if d <= 5 and random() <= 1/d**.8:
                total += 1
    return total


if __name__ == "__main__":
    POPULATION_SIZE = POPULATION_X*POPULATION_Y
    group_infected_count = 0
    avg_day_list = []
    res = simulate(INITIAL_INFECTED_PERSONS, SOCIAL_DISTANCE, "group", 200, simulation_days=SIMULATION_DAYS) #run simulation w/ given parameters